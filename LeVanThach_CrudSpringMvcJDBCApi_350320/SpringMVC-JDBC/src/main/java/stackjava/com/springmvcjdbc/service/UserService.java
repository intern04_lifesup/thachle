package stackjava.com.springmvcjdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stackjava.com.springmvcjdbc.dao.UserDAO;
import stackjava.com.springmvcjdbc.entities.User;

@Service
public class UserService {
	 @Autowired
	  private UserDAO userRepository;
	  
	  public List<User> findAll() {
	    return userRepository.findAll();
	  }
	  public User findById(int id) {
	    return userRepository.findById(id);
	  }
	  
	  public boolean save(User user){
	    if(userRepository.save(user) > 0) {
	    	return true;
	    }
	    return false;  
	  }
	  public boolean update(User user){
	    if(userRepository.update(user) > 0) {
	    	return true;
	    }
		  return false;
	  }
	  
	  public void delete(int id){
	    // validate business
		  userRepository.delete(id);
	  }
}
