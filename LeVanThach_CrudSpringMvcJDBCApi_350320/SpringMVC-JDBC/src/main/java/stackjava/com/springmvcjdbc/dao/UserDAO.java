package stackjava.com.springmvcjdbc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import stackjava.com.springmvcjdbc.entities.User;
import stackjava.com.springmvcjdbc.entities.UserMapper;
@Repository
public class UserDAO {

	@Autowired
	  private JdbcTemplate jdbcTemplate;
	@Transactional
	  public int save(User user) {
	    String sql = "INSERT INTO user (id, age,firstname ,lastname ,password ,salary ,username) VALUES (?,?,?,?,?,?,?)";
	    int result = jdbcTemplate.update(sql, user.getId(), user.getAge(), user.getFirstName(), user.getLastName(), user.getPassword(),
	    		user.getSalary(), user.getUsername());
	   
	    return result;
	  }
	@Transactional
	  public void delete(int id) {
	    String sql = "DELETE FROM user WHERE id = " + id;
	    jdbcTemplate.update(sql);
	  }
	@Transactional
	  public int update(User user) {
	    String sql = "UPDATE user SET age = ?,firstname = ? ,lastname = ? ,password = ? ,salary = ? ,username = ? WHERE id = ? ";
	    int result = jdbcTemplate.update(sql, user.getAge(), user.getFirstName(), user.getLastName(), user.getPassword(),
	    		user.getSalary(), user.getUsername(), user.getId());
	    return result;
	  }
	  public User findById(int id) {
	    String sql = "SELECT * FROM user WHERE id = ?";
	    return jdbcTemplate.queryForObject(sql, new UserMapper(), id);
	  }
	  public List<User> findAll() {
	    String sql = "SELECT * FROM user";
	    return jdbcTemplate.query(sql, new UserMapper());
	  }

}
