package stackjava.com.springmvcjdbc.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class UserMapper implements RowMapper<User>{

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setAge(rs.getInt("age"));
		user.setFirstName(rs.getString("firstname"));
		user.setId(rs.getInt("id"));
		user.setLastName(rs.getString("lastname"));
		user.setPassword(rs.getString("password"));
		user.setSalary(rs.getDouble("salary"));
		user.setUsername(rs.getString("username"));
		return user;
	}

}
