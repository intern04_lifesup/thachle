package myspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import myspring.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	List<User> findByUsername(String name);
}
