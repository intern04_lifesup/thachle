package myspring.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import myspring.entity.User;
import myspring.service.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userService;
	  /* ---------------- GET ALL USER ------------------------ */
	  @RequestMapping(value = "/users", method = RequestMethod.GET)
	  public ResponseEntity<List<User>> getAllUser() {
	    return new ResponseEntity<List<User>>(userService.findAll(), HttpStatus.OK);
	  }
	  /* ---------------- GET USER BY ID ------------------------ */
	  @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	  public ResponseEntity<Object> getUserById(@PathVariable int id) {
	    Optional<User> user = userService.findById(id);
	    if (user != null) {
	      return new ResponseEntity<Object>(user, HttpStatus.OK);
	    }
	    return new ResponseEntity<Object>("Not Found User", HttpStatus.NO_CONTENT);
	  }
	  /* ---------------- CREATE NEW USER ------------------------ */
	  @RequestMapping(value = "/users", method = RequestMethod.POST)
	  public ResponseEntity<String> createUser(@RequestBody User user) {
	    if (userService.add(user)) {
	      return new ResponseEntity<String>("Created!", HttpStatus.CREATED);
	    } else {
	      return new ResponseEntity<String>("User Existed!", HttpStatus.BAD_REQUEST);
	    }
	  }
	  
	  /* ---------------- EDIT USER ------------------------ */
	  @RequestMapping(value = "/users", method = RequestMethod.PUT)
	  public ResponseEntity<String> editUser(@RequestBody User user) {
	    if (userService.update(user)) {
	      return new ResponseEntity<String>("Edited!", HttpStatus.OK);
	    } else {
	      return new ResponseEntity<String>("User Existed!", HttpStatus.BAD_REQUEST);
	    }
	  }
	  /* ---------------- DELETE USER ------------------------ */
	  @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
	  public ResponseEntity<String> deleteUserById(@PathVariable int id) {
	    userService.delete(id);
	    return new ResponseEntity<String>("Deleted!", HttpStatus.OK);
	  }
}
