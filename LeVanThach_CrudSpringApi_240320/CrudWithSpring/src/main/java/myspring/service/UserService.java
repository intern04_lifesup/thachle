package myspring.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myspring.entity.User;
import myspring.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository repository;
	

	public List<User> findAll() {
		return (List<User>) repository.findAll();
	}

	public Optional<User> findById(int id) {
		return repository.findById(id);
	}

	public boolean add(User user) {
		List<User> getUserByName = repository.findByUsername(user.getUsername());
		if(getUserByName.size() != 0) {
			return false;
		}
		repository.save(user);
		return true;
	}
	
	public boolean update(User user) {
		Optional<User> getUser = repository.findById(user.getId());
//		User user1 = getUser.isPresent() ? getUser.get() : null;
		if(getUser.equals(null)) {
			return false;
		}
		
		repository.save(user);
		return true;
	}
	
	public void delete(int id) {
		repository.deleteById(id);
		
	}

	public List<User> loadUserByUsername(String username) {
		return  repository.findByUsername(username);
	}

	
}
